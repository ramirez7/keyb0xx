CC=gcc
LIBS=-levdev
INC=-I/usr/include/libevdev-1.0
HEADERS=config.h values.h

keyb0xx: keyb0xx.c keyboard_device.c $(HEADERS)
	$(CC) $(INC) $^ $(LIBS) -o keyb0xx
