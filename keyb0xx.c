#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <string.h>

#include <libevdev/libevdev.h>
#include <libevdev/libevdev-uinput.h>

#include "keyboard_device.h"
#include "config.h"
#include "values.h"

#define B0XX_BTN_A BTN_0
#define B0XX_BTN_B BTN_1
#define B0XX_BTN_X BTN_2
#define B0XX_BTN_Y BTN_3
#define B0XX_BTN_Z BTN_4
#define B0XX_BTN_L BTN_5
#define B0XX_BTN_R BTN_6
#define B0XX_BTN_START BTN_7
#define B0XX_BTN_DPAD_U BTN_NORTH
#define B0XX_BTN_DPAD_D BTN_SOUTH
#define B0XX_BTN_DPAD_L BTN_WEST
#define B0XX_BTN_DPAD_R BTN_EAST

struct input_absinfo absinfo;

struct libevdev_uinput* get_b0xx_device() {
   struct libevdev* b0xx_dev = libevdev_new();
   
   libevdev_set_name(b0xx_dev, "Key B0XX");
   libevdev_set_id_bustype(b0xx_dev, BUS_USB);
   //libevdev_set_id_product(b0xx_dev, 0x028E);
   //libevdev_set_id_vendor(b0xx_dev, 0x045E);
   libevdev_set_id_version(b0xx_dev, 1);

   libevdev_enable_property(b0xx_dev, INPUT_PROP_BUTTONPAD);

   /* do not remove next 3 lines or udev scripts won't assign 0664 permissions -sh */
   libevdev_enable_event_type(b0xx_dev, EV_KEY);
   libevdev_enable_event_code(b0xx_dev, EV_KEY, BTN_JOYSTICK, NULL);
   libevdev_enable_event_code(b0xx_dev, EV_KEY, BTN_TRIGGER, NULL);

   absinfo.maximum = MAX_ANALOG;
   absinfo.minimum = MIN_ANALOG;
   absinfo.resolution = 1;
   absinfo.value = 0;
   absinfo.flat = 1;
   absinfo.fuzz = 0;
   
   libevdev_enable_event_code(b0xx_dev, EV_KEY, B0XX_BTN_A, 0);
   libevdev_enable_event_code(b0xx_dev, EV_KEY, B0XX_BTN_B, 0);
   libevdev_enable_event_code(b0xx_dev, EV_KEY, B0XX_BTN_X, 0);
   libevdev_enable_event_code(b0xx_dev, EV_KEY, B0XX_BTN_Y, 0);
   libevdev_enable_event_code(b0xx_dev, EV_KEY, B0XX_BTN_Z, 0);
   libevdev_enable_event_code(b0xx_dev, EV_KEY, B0XX_BTN_L, 0);
   libevdev_enable_event_code(b0xx_dev, EV_KEY, B0XX_BTN_R, 0);
   libevdev_enable_event_code(b0xx_dev, EV_KEY, B0XX_BTN_START, 0);
   libevdev_enable_event_code(b0xx_dev, EV_KEY, B0XX_BTN_DPAD_U, 0);
   libevdev_enable_event_code(b0xx_dev, EV_KEY, B0XX_BTN_DPAD_D, 0);
   libevdev_enable_event_code(b0xx_dev, EV_KEY, B0XX_BTN_DPAD_L, 0);
   libevdev_enable_event_code(b0xx_dev, EV_KEY, B0XX_BTN_DPAD_R, 0);

   libevdev_enable_event_type(b0xx_dev, EV_ABS);
   libevdev_enable_event_code(b0xx_dev, EV_ABS, ABS_X, &absinfo);
   libevdev_enable_event_code(b0xx_dev, EV_ABS, ABS_Y, &absinfo);
   libevdev_enable_event_code(b0xx_dev, EV_ABS, ABS_RX, &absinfo);
   libevdev_enable_event_code(b0xx_dev, EV_ABS, ABS_RY, &absinfo);

   struct libevdev_uinput *uidev_b0xx;
   
   int err = libevdev_uinput_create_from_device(b0xx_dev,
					 LIBEVDEV_UINPUT_OPEN_MANAGED,
					 &uidev_b0xx);   

   if(err != 0) {
      fprintf(stderr, "Could not create keyb0xx device");
      exit(1);
   }

  return uidev_b0xx; 
}

int write_event(struct libevdev_uinput *uidev, int type, int code, int value) {
   int err = libevdev_uinput_write_event(uidev, type, code, value);
   if(err != 0)
	   return err;
   // Trigger reporting of event
   err = libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
   if(err != 0)
	   return err;
   return 0;
}

// Get inputs from keyboard and update input state
void parse_event(struct input_event *ev, input_state *is) {
   /* Debug info	
   printf("Event: %s %s %d\n\n",
	  libevdev_event_type_get_name(ev->type),
	  libevdev_event_code_get_name(ev->type, ev->code),
	  ev->value);
   */

   int code = ev->code;
   int value = ev->value; // 1 == press; 0 == unpress
   switch(code) {
      case INPUT_KEY_A:
	 if(value == 1) 
	   is->a_held = true;
	 else if(value == 0)
           is->a_held = false;
	 break;
      case INPUT_KEY_B:
	 if(value == 1) 
	   is->b_held = true;
	 else if(value == 0)
           is->b_held = false;
	 break;
      case INPUT_KEY_X:
	 if(value == 1) 
	   is->x_held = true;
	 else if(value == 0)
           is->x_held = false;
	 break;
      case INPUT_KEY_Y:
	 if(value == 1) 
	   is->y_held = true;
	 else if(value == 0)
           is->y_held = false;
	 break;
      case INPUT_KEY_Z:
	 if(value == 1) 
	   is->z_held = true;
	 else if(value == 0)
           is->z_held = false;
	 break;
      case INPUT_KEY_L:
	 if(value == 1) 
	   is->l_held = true;
	 else if(value == 0)
           is->l_held = false;
	 break;
      case INPUT_KEY_R:
	 if(value == 1) 
	   is->r_held = true;
	 else if(value == 0)
           is->r_held = false;
	 break;
      case INPUT_KEY_START:
	 if(value == 1) 
	   is->start_held = true;
	 else if(value == 0)
           is->start_held = false;
	 break;
      case INPUT_KEY_UP:
	 if(value == 1) {
	   is->up_held = true;
	   is->up_last_held = true;
	 }
	 else if(value == 0)
           is->up_held = false;
	 break;
      case INPUT_KEY_DOWN:
	 if(value == 1) {
	   is->down_held = true;
	   is->up_last_held = false;
	 }
	 else if(value == 0)
           is->down_held = false;
	 break;
      case INPUT_KEY_LEFT:
	 if(value == 1) {
	   is->left_held = true;
	   is->left_last_held = true;
	 }
	 else if(value == 0)
           is->left_held = false;
	 break;
      case INPUT_KEY_RIGHT:
	 if(value == 1) {
	   is->right_held = true;
	   is->left_last_held = false;
	 }
	 else if(value == 0)
           is->right_held = false;
	 break;
      case INPUT_KEY_C_UP:
	 if(value == 1) {
	   is->c_up_held = true;
	   is->c_up_last_held = true;
	 }
	 else if(value == 0)
           is->c_up_held = false;
	 break;
      case INPUT_KEY_C_DOWN:
	 if(value == 1) {
	   is->c_down_held = true;
	   is->c_up_last_held = false;
	 }
	 else if(value == 0)
           is->c_down_held = false;
	 break;
      case INPUT_KEY_C_LEFT:
	 if(value == 1) {
	   is->c_left_held = true;
	   is->c_left_last_held = true;
	 }
	 else if(value == 0)
           is->c_left_held = false;
	 break;
      case INPUT_KEY_C_RIGHT:
	 if(value == 1) {
	   is->c_right_held = true;
	   is->c_left_last_held = false;
	 }
	 else if(value == 0)
           is->c_right_held = false;
	 break;
      case INPUT_KEY_MOD_X:
	 is->modX_held = value;
	 break;
      case INPUT_KEY_MOD_Y:
	 is->modY_held = value;
	 break;
   }
}

// Sends the GCC controls to the keyb0xx device
// May the comments help you on your way though a jungle of if, else if, and else's
void output_controls(input_state *is, struct libevdev_uinput* b0xx_dev) {
   bool modX_active = is->modX_held && !is->modY_held;
   bool modY_active = is->modY_held && !is->modX_held;

   // Change analog for auto shield tilt (shield drop etc)
   bool auto_shield_active = is->l_held || is->z_held;

   // Control stick handling
   if(is->up_held && is->up_last_held) {
      if(modX_active) { // Mod X held
	 if(is->l_held || is->r_held) { // Nerf airdodge angles
            write_event(b0xx_dev, EV_ABS, ABS_Y, modX_airdodge_angles.y_pos);
	 }
	 // Extra firefox angles holding c-stick directions
	 else if(is->c_up_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_Y, modX_c_stick_angles.c_up_y_pos);
	 } else if(is->c_down_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_Y, modX_c_stick_angles.c_down_y_pos);
	 } else if(is->c_left_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_Y, modX_c_stick_angles.c_left_y_pos);
	 } else if(is->c_right_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_Y, modX_c_stick_angles.c_right_y_pos);
	 } else {
	    write_event(b0xx_dev, EV_ABS, ABS_Y, modX_angles.y_pos);
	 }
      } else if(modY_active) { // Mod Y held
	 if(is->l_held || is->r_held) { // Nerf airdodge angles
            write_event(b0xx_dev, EV_ABS, ABS_Y, modY_airdodge_angles.y_pos);
	 }
	 // Extra firefox angles holding c-stick directions
	 else if(is->c_up_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_Y, modY_c_stick_angles.c_up_y_pos);
	 } else if(is->c_down_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_Y, modY_c_stick_angles.c_down_y_pos);
	 } else if(is->c_left_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_Y, modY_c_stick_angles.c_left_y_pos);
	 } else if(is->c_right_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_Y, modY_c_stick_angles.c_right_y_pos);
	 } else {
	    write_event(b0xx_dev, EV_ABS, ABS_Y, modY_angles.y_pos);
	 }
      } else if(auto_shield_active && (is->left_held || is->right_held)) { // Q1/Q2 auto shield tilt
	 write_event(b0xx_dev, EV_ABS, ABS_Y, auto_shield_angles.y_Q1Q2);
      } else { // No modifier
	 write_event(b0xx_dev, EV_ABS, ABS_Y, absinfo.maximum);
      }
   } else if(!is->up_held && is->up_last_held) { // Letting go of control stick
      write_event(b0xx_dev, EV_ABS, ABS_Y, absinfo.value);
   }

   if(is->down_held && !is->up_last_held) {
      if(modX_active) { // Mod X held
	 if(is->l_held || is-> r_held) { // Nerf airdodge angles
            write_event(b0xx_dev, EV_ABS, ABS_Y, modX_airdodge_angles.y_neg);
	 }
	 // Extra firefox angles holding c-stick directions
	 else if(is->c_up_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_Y, modX_c_stick_angles.c_up_y_neg);
	 } else if(is->c_down_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_Y, modX_c_stick_angles.c_down_y_neg);
	 } else if(is->c_left_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_Y, modX_c_stick_angles.c_left_y_neg);
	 } else if(is->c_right_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_Y, modX_c_stick_angles.c_right_y_neg);
	 } else {
	    write_event(b0xx_dev, EV_ABS, ABS_Y, modX_angles.y_neg);
	 }
      } else if(modY_active) { // Mod Y held
	 if(is->l_held || is->r_held) { // Nerf airdodge angles
            write_event(b0xx_dev, EV_ABS, ABS_Y, modY_airdodge_angles.y_neg);
	 }
	 // Extra firefox angles holding c-stick directions
	 else if(is->c_up_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_Y, modY_c_stick_angles.c_up_y_neg);
	 } else if(is->c_down_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_Y, modY_c_stick_angles.c_down_y_neg);
	 } else if(is->c_left_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_Y, modY_c_stick_angles.c_left_y_neg);
	 } else if(is->c_right_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_Y, modY_c_stick_angles.c_right_y_neg);
	 } else {
	    write_event(b0xx_dev, EV_ABS, ABS_Y, modY_angles.y_neg);
	 }
      } else if(auto_shield_active && (is->left_held || is->right_held)) { // Q3/Q4 auto shield tilt
	 write_event(b0xx_dev, EV_ABS, ABS_Y, auto_shield_angles.y_Q3Q4);
      } else { // No modifier
	 write_event(b0xx_dev, EV_ABS, ABS_Y, absinfo.minimum);
      }
   } else if(!is->down_held && !is->up_last_held) { // Letting go of control stick
      write_event(b0xx_dev, EV_ABS, ABS_Y, absinfo.value);
   }

   if(is->left_held && is->left_last_held) {
      if(is->b_held) { // Holding B always gives +/-1.0 X value (reverse neutral b integrity)
         write_event(b0xx_dev, EV_ABS, ABS_X, absinfo.minimum);
      } else if(is->right_held && !(is->l_held || is->r_held)) { // Ledge dash optimization
         write_event(b0xx_dev, EV_ABS, ABS_X, absinfo.minimum);
      } else if(modX_active) {
	 if(is->l_held || is->r_held) { // Nerf airdodge angles
	    if(is->up_held) {
               write_event(b0xx_dev, EV_ABS, ABS_X, modX_airdodge_angles.x_Q2);
	    } else if(is->down_held) {
               write_event(b0xx_dev, EV_ABS, ABS_X, modX_airdodge_angles.x_Q3);
	    } else {
	       write_event(b0xx_dev, EV_ABS, ABS_X, modX_angles.x_mid_neg); // Mod X && !(U/D held)
	    }
	 }
	 // Extra firefox angles holding c-stick directions
	 else if(is->c_up_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_X, modX_c_stick_angles.c_up_x_neg);
	 } else if(is->c_down_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_X, modX_c_stick_angles.c_down_x_neg);
	 } else if(is->c_left_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_X, modX_c_stick_angles.c_left_x_neg);
	 } else if(is->c_right_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_X, modX_c_stick_angles.c_right_x_neg);
	 } else {
	    if(is->up_held || is->down_held)
	       write_event(b0xx_dev, EV_ABS, ABS_X, modX_angles.x_Q2Q3);    // Mod X && U/D held
	    else
	       write_event(b0xx_dev, EV_ABS, ABS_X, modX_angles.x_mid_neg); // Mod X && !(U/D held)
	 }
      } else if(modY_active) {
	 if(is->l_held || is->r_held) { // Nerf airdodge angles
	    if(is->up_held) {
               write_event(b0xx_dev, EV_ABS, ABS_X, modY_airdodge_angles.x_Q2);
	    } else if(is->down_held) {
               write_event(b0xx_dev, EV_ABS, ABS_X, modY_airdodge_angles.x_Q3);
	    } else {
	       write_event(b0xx_dev, EV_ABS, ABS_X, modY_angles.x_mid_neg); // Mod X && !(U/D held)
	    }
	 }
	 // Extra firefox angles holding c-stick directions
	 else if(is->c_up_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_X, modY_c_stick_angles.c_up_x_neg);
	 } else if(is->c_down_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_X, modY_c_stick_angles.c_down_x_neg);
	 } else if(is->c_left_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_X, modY_c_stick_angles.c_left_x_neg);
	 } else if(is->c_right_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_X, modY_c_stick_angles.c_right_x_neg);
	 } else {
	    if(is->up_held || is->down_held)
	       write_event(b0xx_dev, EV_ABS, ABS_X, modY_angles.x_Q2Q3);    // Mod Y && U/D held
	    else
	       write_event(b0xx_dev, EV_ABS, ABS_X, modY_angles.x_mid_neg); // Mod Y && !(U/D held)
	 }
      } else if(auto_shield_active && is->up_held) { // Q2 auto shield tilt
	 write_event(b0xx_dev, EV_ABS, ABS_X, auto_shield_angles.x_Q1Q2_neg);
      } else if(auto_shield_active && is->down_held) { // Q3 auto shield tilt
	 write_event(b0xx_dev, EV_ABS, ABS_X, auto_shield_angles.x_Q3Q4_neg);
      } else { // No modifier
	 write_event(b0xx_dev, EV_ABS, ABS_X, absinfo.minimum);
      }
   } else if(!is->left_held && is->left_last_held) { // Letting go of control stick
      write_event(b0xx_dev, EV_ABS, ABS_X, absinfo.value);
   }

   if(is->right_held && !is->left_last_held) {
      if(is->b_held) { // Holding B always gives +/-1.0 X value (reverse neutral b integrity)
         write_event(b0xx_dev, EV_ABS, ABS_X, absinfo.maximum);
      }
      else if(is->left_held && !(is->l_held || is->r_held)) { // Ledge dash optimization
         write_event(b0xx_dev, EV_ABS, ABS_X, absinfo.maximum);
      } else if(modX_active) {
	 if(is->l_held || is->r_held) { // Nerf airdodge angles
	    if(is->up_held) {
               write_event(b0xx_dev, EV_ABS, ABS_X, modX_airdodge_angles.x_Q1);
	    } else if(is->down_held) {
               write_event(b0xx_dev, EV_ABS, ABS_X, modX_airdodge_angles.x_Q4);
	    } else {
	       write_event(b0xx_dev, EV_ABS, ABS_X, modX_angles.x_mid_pos); // Mod X && !(U/D held)
	    }
	 }
	 // Extra firefox angles holding c-stick directions
	 else if(is->c_up_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_X, modX_c_stick_angles.c_up_x_pos);
	 } else if(is->c_down_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_X, modX_c_stick_angles.c_down_x_pos);
	 } else if(is->c_left_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_X, modX_c_stick_angles.c_left_x_pos);
	 } else if(is->c_right_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_X, modX_c_stick_angles.c_right_x_pos);
	 } else {
	    if(is->up_held || is->down_held)
	       write_event(b0xx_dev, EV_ABS, ABS_X, modX_angles.x_Q1Q4);    // Mod X && U/D held
	    else
	       write_event(b0xx_dev, EV_ABS, ABS_X, modX_angles.x_mid_pos); // Mod X && !(U/D held)
	 }
      } else if(modY_active) {
	 if(is->l_held || is->r_held) { // Nerf airdodge angles
	    if(is->up_held) {
               write_event(b0xx_dev, EV_ABS, ABS_X, modY_airdodge_angles.x_Q1);
	    } else if(is->down_held) {
               write_event(b0xx_dev, EV_ABS, ABS_X, modY_airdodge_angles.x_Q4);
	    } else {
	       write_event(b0xx_dev, EV_ABS, ABS_X, modY_angles.x_mid_pos); // Mod X && !(U/D held)
	    }
	 }
	 // Extra firefox angles holding c-stick directions
	 else if(is->c_up_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_X, modY_c_stick_angles.c_up_x_pos);
	 } else if(is->c_down_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_X, modY_c_stick_angles.c_down_x_pos);
	 } else if(is->c_left_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_X, modY_c_stick_angles.c_left_x_pos);
	 } else if(is->c_right_held) {
	    write_event(b0xx_dev, EV_ABS, ABS_X, modY_c_stick_angles.c_right_x_pos);
	 } else {
	    if(is->up_held || is->down_held)
	       write_event(b0xx_dev, EV_ABS, ABS_X, modY_angles.x_Q1Q4);    // Mod Y && U/D held
	    else
	       write_event(b0xx_dev, EV_ABS, ABS_X, modY_angles.x_mid_pos); // Mod Y && !(U/D held)
	 }
      } else if(auto_shield_active && is->up_held) { // Q1 auto shield tilt
         write_event(b0xx_dev, EV_ABS, ABS_X, auto_shield_angles.x_Q1Q2_pos);
      } else if(auto_shield_active && is->down_held) { // Q4 auto shield tilt
         write_event(b0xx_dev, EV_ABS, ABS_X, auto_shield_angles.x_Q3Q4_pos);
      } else { // No modifier
	 write_event(b0xx_dev, EV_ABS, ABS_X, absinfo.maximum);
      }
   } else if(!is->right_held && !is->left_last_held) { // Letting go of control stick
      write_event(b0xx_dev, EV_ABS, ABS_X, absinfo.value);
   }

   // C stick handling
   if(is->c_up_held && is->c_up_last_held) {
      if(is->modX_held && is->modY_held) { // Both modifiers cause dpad inputs
         write_event(b0xx_dev, EV_KEY, B0XX_BTN_DPAD_U, 1);
         write_event(b0xx_dev, EV_ABS, ABS_RY, absinfo.value); // Make sure c-stick is not active (only dpad input)
      } else {
         write_event(b0xx_dev, EV_ABS, ABS_RY, absinfo.maximum);
         write_event(b0xx_dev, EV_KEY, B0XX_BTN_DPAD_U, 0);
      }
   }
   else if(!is->c_up_held) {
      write_event(b0xx_dev, EV_KEY, B0XX_BTN_DPAD_U, 0);
      if(is->c_up_last_held) { // C stick should be neutral
         write_event(b0xx_dev, EV_ABS, ABS_RY, absinfo.value);
      }
   }

   if(is->c_down_held && !is->c_up_last_held) {
      if(is->modX_held && is->modY_held) { // Both modifiers cause dpad inputs
         write_event(b0xx_dev, EV_KEY, B0XX_BTN_DPAD_D, 1);
         write_event(b0xx_dev, EV_ABS, ABS_RY, absinfo.value); // Make sure c-stick is not active (only dpad input)
      } else {
         write_event(b0xx_dev, EV_ABS, ABS_RY, absinfo.minimum);
         write_event(b0xx_dev, EV_KEY, B0XX_BTN_DPAD_D, 0);
      }
   }
   else if(!is->c_down_held) {
      write_event(b0xx_dev, EV_KEY, B0XX_BTN_DPAD_D, 0);
      if(!is->c_up_last_held) { // C stick should be neutral
         write_event(b0xx_dev, EV_ABS, ABS_RY, absinfo.value);
      }
   }

   if(is->c_left_held && is->c_left_last_held) {
      if(is->modX_held && is->modY_held) { // Both modifiers cause dpad inputs
         write_event(b0xx_dev, EV_KEY, B0XX_BTN_DPAD_L, 1);
         write_event(b0xx_dev, EV_ABS, ABS_RX, absinfo.value); // Make sure c-stick is not active (only dpad input)
      } else {
         write_event(b0xx_dev, EV_ABS, ABS_RX, absinfo.minimum);
         write_event(b0xx_dev, EV_KEY, B0XX_BTN_DPAD_L, 0);
      }
   }
   else if(!is->c_left_held) {
      write_event(b0xx_dev, EV_KEY, B0XX_BTN_DPAD_L, 0);
      if(is->c_left_last_held) { // C stick should be neutral
         write_event(b0xx_dev, EV_ABS, ABS_RX, absinfo.value);
      }
   }

   if(is->c_right_held && !is->c_left_last_held) {
      if(is->modX_held && is->modY_held) { // Both modifiers cause dpad inputs
         write_event(b0xx_dev, EV_KEY, B0XX_BTN_DPAD_R, 1);
         write_event(b0xx_dev, EV_ABS, ABS_RX, absinfo.value); // Make sure c-stick is not active (only dpad input)
      } else {
         write_event(b0xx_dev, EV_ABS, ABS_RX, absinfo.maximum);
         write_event(b0xx_dev, EV_KEY, B0XX_BTN_DPAD_R, 0);
      }
   }
   else if(!is->c_right_held) {
      write_event(b0xx_dev, EV_KEY, B0XX_BTN_DPAD_R, 0);
      if(!is->c_left_last_held) { // C stick should be neutral
         write_event(b0xx_dev, EV_ABS, ABS_RX, absinfo.value);
      }
   }

   // Button handling
   if(is->a_held)
      write_event(b0xx_dev, EV_KEY, B0XX_BTN_A, 1);
   else
      write_event(b0xx_dev, EV_KEY, B0XX_BTN_A, 0);
   if(is->b_held)
      write_event(b0xx_dev, EV_KEY, B0XX_BTN_B, 1);
   else
      write_event(b0xx_dev, EV_KEY, B0XX_BTN_B, 0);
   if(is->x_held)
      write_event(b0xx_dev, EV_KEY, B0XX_BTN_X, 1);
   else
      write_event(b0xx_dev, EV_KEY, B0XX_BTN_X, 0);
   if(is->y_held)
      write_event(b0xx_dev, EV_KEY, B0XX_BTN_Y, 1);
   else
      write_event(b0xx_dev, EV_KEY, B0XX_BTN_Y, 0);
   if(is->z_held)
      write_event(b0xx_dev, EV_KEY, B0XX_BTN_Z, 1);
   else
      write_event(b0xx_dev, EV_KEY, B0XX_BTN_Z, 0);
   if(is->l_held)
      write_event(b0xx_dev, EV_KEY, B0XX_BTN_L, 1);
   else
      write_event(b0xx_dev, EV_KEY, B0XX_BTN_L, 0);
   if(is->r_held)
      write_event(b0xx_dev, EV_KEY, B0XX_BTN_R, 1);
   else
      write_event(b0xx_dev, EV_KEY, B0XX_BTN_R, 0);
   if(is->start_held)
      write_event(b0xx_dev, EV_KEY, B0XX_BTN_START, 1);
   else
      write_event(b0xx_dev, EV_KEY, B0XX_BTN_START, 0);
}	

int main() {
   struct libevdev* keyboard = get_keyboard_device();
   struct libevdev_uinput* b0xx_dev = get_b0xx_device();

   printf("Successfully created keyb0xx device. Play some melee!\n");
   fflush(stdout);

   // Defaults input_state to all false values
   INIT_INPUT_STATE(input_state);

   int rc = 1;
    
   do {
	   struct input_event ev;
	   // Get input events from keyboard (must be blocking or else some events may not process!)
	   rc = libevdev_next_event(keyboard, LIBEVDEV_READ_FLAG_BLOCKING, &ev);
	   if (rc == 0) {
		parse_event(&ev, &input_state);
		output_controls(&input_state, b0xx_dev);
	   }

   } while (rc == 1 || rc == 0);
}
