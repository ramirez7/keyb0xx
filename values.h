#include <stdbool.h>

// Structure for holding the current state of buttons being pressed.
// 
// Having the state held in this way allows for modifiers to be retroactively activated, among
// any other buttons.
typedef struct {
  bool up_held;
  bool down_held;
  bool left_held;
  bool right_held;
  bool c_up_held;
  bool c_down_held;
  bool c_left_held;
  bool c_right_held;
  bool a_held;
  bool b_held;
  bool x_held;
  bool y_held;
  bool z_held;
  bool l_held;
  bool r_held;
  bool start_held;
  bool modX_held;
  bool modY_held;
  bool up_last_held;    // false -> down was last held
  bool left_last_held;  // false -> right was last held
  bool c_up_last_held;
  bool c_left_last_held;
} input_state;

// Initial input state should be all false (no buttons pressed)
#define INIT_INPUT_STATE(X) input_state X = {false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false}

// Structure for holding the analog values to send when holding a modifier button
//
// Note: Although some of the coordinates are mirrored,
//   positive and negative values differ since there are 127 negative values and 128 positive
//   on a gamecube controller analog stick.
//
typedef struct {
  int y_pos;     // Vertical up position      (X = 0)
  int y_neg;     // Vertical down position    (X = 0)
  int x_mid_pos; // Horizontal right position (Y = 0)
  int x_mid_neg; // Horizontal left position  (Y = 0)
  int x_Q1Q4;    // Right quadrants X position
  int x_Q2Q3;    // Left quadrants X position
} mod_angles;

// When airdodging, steepness of angles are reduced to simluate difficulty of steep airdodges
// on a gamecube controller
//
// Up angles are slightly different due to an exploitative Peach technique
typedef struct {
  int x_Q1;
  int x_Q2;
  int x_Q3;
  int x_Q4;
  int y_pos;
  int y_neg;
} airdodge_angles;

// Extra firefox angles with c-stick direction + modifier
// Each x, y value specified since none are the same for any c-stick direction
typedef struct {
  int c_up_x_pos;
  int c_up_y_pos;
  int c_up_x_neg;
  int c_up_y_neg;
  int c_down_x_pos;
  int c_down_y_pos;
  int c_down_x_neg;
  int c_down_y_neg;
  int c_left_x_pos;
  int c_left_y_pos;
  int c_left_x_neg;
  int c_left_y_neg;
  int c_right_x_pos;
  int c_right_y_pos;
  int c_right_x_neg;
  int c_right_y_neg;
} c_stick_mod_angles;

// When holding shield buttons (L/Z) analog values are modified to allow shield dropping on vanilla
// melee, tilting shield up-left and up-right without jumping.
// Only diagonal values are changed. Cardinals remain +/-1.0
// Z button also prompts these angle changes.
typedef struct {
  int y_Q1Q2;
  int x_Q1Q2_pos;
  int x_Q1Q2_neg;
  int y_Q3Q4;
  int x_Q3Q4_pos;
  int x_Q3Q4_neg;
} auto_shield_override_angles;

// Full analog direction values
#define MIN_ANALOG (-(1 << 16))
#define MAX_ANALOG (1 << 16)

// Analog values for modifier X
const mod_angles modX_angles = {
  MAX_ANALOG * 0.185,
  -(MAX_ANALOG* 0.18),
  MAX_ANALOG * 0.42,
  -(MAX_ANALOG * 0.41),
  MAX_ANALOG * .47,
  -(MAX_ANALOG * .46),
};

// Analog values for modifier Y
const mod_angles modY_angles = {
  MAX_ANALOG * 0.47,
  -(MAX_ANALOG * 0.46),
  MAX_ANALOG * 0.22,
  -(MAX_ANALOG * 0.21),
  MAX_ANALOG * .185,
  -(MAX_ANALOG * .18),
};

// Analog values for modifier X + c-stick
const c_stick_mod_angles modX_c_stick_angles = {
  MAX_ANALOG * 0.42,    // up_x_pos
  MAX_ANALOG * 0.295,   // up_y_pos
  -(MAX_ANALOG * 0.41), // up_x_neg
  -(MAX_ANALOG * 0.29), // up_y_neg
  MAX_ANALOG * .49,     // down_x_pos
  MAX_ANALOG * .24,     // down_y_pos
  -(MAX_ANALOG * .485), // down_x_neg
  -(MAX_ANALOG * .235), // down_y_neg
  MAX_ANALOG * 0.498,   // left_x_pos
  MAX_ANALOG * 0.295,   // left_y_pos
  -(MAX_ANALOG * 0.49), // left_x_neg
  -(MAX_ANALOG * 0.29), // left_y_neg
  MAX_ANALOG * .405,    // right_x_pos
  MAX_ANALOG * .335,    // right_y_pos
  -(MAX_ANALOG * .40),  // right_x_neg
  -(MAX_ANALOG * .33),  // right_y_neg
};

// Analog values for modifier Y + c-stick
const c_stick_mod_angles modY_c_stick_angles = {
  MAX_ANALOG * 0.35,    // up_x_pos
  MAX_ANALOG * 0.501,   // up_y_pos
  -(MAX_ANALOG * 0.345),// up_x_neg
  -(MAX_ANALOG * 0.49), // up_y_neg
  MAX_ANALOG * .25,     // down_x_pos
  MAX_ANALOG * .51,     // down_y_pos
  -(MAX_ANALOG * .24),  // down_x_neg
  -(MAX_ANALOG * .50),  // down_y_neg
  MAX_ANALOG * 0.295,   // left_x_pos
  MAX_ANALOG * 0.497,   // left_y_pos
  -(MAX_ANALOG * 0.29), // left_x_neg
  -(MAX_ANALOG * 0.49), // left_y_neg
  MAX_ANALOG * .375,    // right_x_pos
  MAX_ANALOG * .455,    // right_y_pos
  -(MAX_ANALOG * .37),  // right_x_neg
  -(MAX_ANALOG * .445), // right_y_neg
};

const auto_shield_override_angles auto_shield_angles = {
  MAX_ANALOG * 0.34,    // y_Q1Q2
  MAX_ANALOG * 0.34,    // x_Q1Q2_pos
  -(MAX_ANALOG * 0.335),// x_Q1Q2_neg
  -(MAX_ANALOG * 0.43), // y_Q3Q4
  MAX_ANALOG * 0.45,    // x_Q3Q4_pos
  -(MAX_ANALOG * 0.445),// x_Q3Q4_neg
};

const airdodge_angles modX_airdodge_angles = {
  MAX_ANALOG * 0.80,   // x_Q1
  -(MAX_ANALOG* 0.78), // x_Q2
  -(MAX_ANALOG* 0.395), // x_Q3
  MAX_ANALOG * 0.405,   // x_Q4
  MAX_ANALOG * 0.44,   // y_pos
  -(MAX_ANALOG * 0.23),// y_neg
};

const airdodge_angles modY_airdodge_angles = {
  MAX_ANALOG * 0.32,   // x_Q1
  -(MAX_ANALOG* 0.31), // x_Q2
  -(MAX_ANALOG* 0.31), // x_Q3
  MAX_ANALOG * 0.32,   // x_Q4
  MAX_ANALOG * 0.58,   // y_pos
  -(MAX_ANALOG * 0.53),// y_neg
};
